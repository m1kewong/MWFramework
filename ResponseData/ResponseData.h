//
//  MapData.h
//  Baphomet
//
//  Created by Mike Wong on 21/8/15.
//  Copyright (c) 2015 Mike Wong. All rights reserved.
//

#import "MWObject.h"

@interface ResponseData : MWObject

//For List
@property (nonatomic) NSUInteger page;
@property (nonatomic) NSUInteger numPages;
@property (nonatomic) NSUInteger numRecord;
@property (nonatomic, strong) NSArray *records;

//For Map
@property (nonatomic) NSUInteger zoomLevel;
@property (nonatomic, strong) NSArray *mapPoints;

@end
