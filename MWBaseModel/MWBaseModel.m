//
//  BaseModel.m
//
//  Created by Mike Wong on 8/6/13.
//  Copyright (c) 2013 Ricacorp. All rights reserved.
//

#import "MWBaseModel.h"

#import "AFNetworking.h"
#import "AFOAuth2Manager/AFOAuth2Manager.h"
#import "AFOAuth2Manager/AFHTTPRequestSerializer+OAuth2.h"

//Class
#import "MWObject.h"
#import "ResponseData.h"
#import "MWRepository.h"

#define LOADINGBATCHSIZE 15

@interface MWBaseModel ()

@property (nonatomic, strong) NSString *SERVICEPROVIDER;
@property (nonatomic, strong) MWRepository *myRepository;

//Backing data storage
@property (nonatomic, strong) NSArray *results;

- (void)privateGetDataByName:(NSString *)name
                    usingURL:(NSString *)urlString
             completionBlock:(void(^)(NSArray *))block;

@end

@implementation MWBaseModel

#pragma mark Private
- (void)privateGetDataByName:(NSString *)name
                    usingURL:(NSString *)urlString
             completionBlock:(void(^)(NSArray *))block {
    //Mutable feed for adding
    NSMutableArray *mutatableFeed = [[NSMutableArray alloc]init];
    NSString *IDEnabledURL = urlString;
    NSLog(@"%@", IDEnabledURL);
    //URL encode it
    IDEnabledURL = [IDEnabledURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setAuthorizationHeaderFieldWithCredential:
     [AFOAuthCredential retrieveCredentialWithIdentifier:self.SERVICEPROVIDER]];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [manager GET:IDEnabledURL parameters:nil progress:nil
         success:^(NSURLSessionTask *task, id responseObject) {
             //To handle different return data type
             NSArray *posts = nil;
             
             if ([responseObject isKindOfClass:[NSArray class]]) {
                 posts = (NSArray *) responseObject;
             }
             
             else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                 if ([responseObject allKeys].count != 0) {
                     //Check which is it a list or a single response
                     if ([responseObject valueForKey:@"results"]) {
                         posts = [responseObject valueForKey:@"results"];
                     }
                     else {
                         posts = @[responseObject];
                     }
                 }
             }
             //1. Results part
             //Error handling
             if (posts == (id)[NSNull null]) {
                 //Return nil array if error
                 block(nil);
                 return;
             }
             //NSLog(@"%@", posts);
             //Loop the result sets
             for (int j = 0; j < posts.count; j++) {
                 //Use auto release pool in loop to reduce memory usage
                 @autoreleasepool {
                     NSDictionary *d = [posts objectAtIndex:j];
                     id t = [[NSClassFromString(self.myRepository.ClassName)alloc]
                             initWithAttributes:d];
                     [mutatableFeed addObject:t];
                 }
             }
             //NSLog(@"Count: %i", temp.count);
             
             //2. Map points part
             ResponseData *rd = [[ResponseData alloc]init];
             if ([responseObject isKindOfClass:[NSDictionary class]]) {
                 NSArray *mapPoints = [responseObject valueForKey:@"mapPoints"];
                 
                 //2.1 Development and team part
                 NSArray *developments = [responseObject valueForKey:@"developments"];
                 NSArray *teams = [responseObject valueForKey:@"teams"];
                 
                 NSMutableArray *points = [[NSMutableArray alloc]init];
                 void (^prepareData)(NSArray *) = ^(NSArray *ary) {
                     //Error handling
                     if (ary != (id)[NSNull null] && ary.count > 0) {
                         @autoreleasepool {
                             for (NSDictionary *dict in ary) {
                                 id p = [[NSClassFromString(@"Tilev3")alloc]
                                         initWithAttributes:dict];
                                 [points addObject:p];
                             }
                         }
                     }
                 };
                 
                 prepareData(mapPoints);
                 prepareData(developments);
                 prepareData(teams);
                 
                 rd.zoomLevel = [[responseObject valueForKey:@"zoom"]integerValue];
                 rd.mapPoints = [NSArray arrayWithArray:points];
             }
             
             //3. Prepare for the return
             NSArray *retAry = nil;
             if ([responseObject isKindOfClass:[NSDictionary class]]
                 && [responseObject valueForKey:@"results"]) {
                 rd.records = [NSArray arrayWithArray:mutatableFeed];
                 rd.numRecord = [[responseObject valueForKey:@"total"]integerValue];
                 retAry = @[rd];
             }
             //Straightly array is default case
             else {
                 retAry = [NSArray arrayWithArray:mutatableFeed];
             }
             
             block(retAry);
         }
         failure:^(NSURLSessionTask *task, NSError *error) {
             // Handle error
             NSLog(@"%@", [error userInfo]);
             block(nil);
         }];
}

#pragma mark Contructor
- (id)initWithRepository:(MWRepository *)repository
   withPackageIdentifier:(NSString *)identifier {
    self = [super init];
    if (self) {
        self.myRepository = repository;
        self.SERVICEPROVIDER = identifier;
    }
    return self;
}

#pragma mark Default methods
- (void)getDefaultListingWithExtraPara:(NSString *)para
                          fromBegining:(BOOL)isFromBegining {
    //Mark the flag
    self.updating = YES;
    
    //Reset storage
    if (isFromBegining) {
        self.results = nil;
        self.reachedTheEnd = NO;
    }
    else {
        self.gettingMore = YES;
    }
    
    //Block for getting more items
    void (^update)(NSArray *) = ^(NSArray *ary) {
        //Using an autorelease pool to reduce the memory footage
        //however it would cause the issue when record > 200 on simulator
        @autoreleasepool {
            if (ary && ary.count > 0) {
                if (!isFromBegining) {
                    //Handle appending of data
                    //Append to the response data
                    if ([self.results.firstObject isKindOfClass:[ResponseData class]]) {
                        ResponseData *rd = self.results.firstObject;
                        NSMutableArray *t = [NSMutableArray arrayWithArray:rd.records];
                        
                        NSArray *tPtr2 = [ary.firstObject isKindOfClass:[ResponseData class]]?
                        ((ResponseData *)ary.firstObject).records:ary;
                        [t addObjectsFromArray:tPtr2];
                        
                        rd.records = [NSArray arrayWithArray:t];
                    }
                    //Directly append to the results
                    else {
                        NSMutableArray *t = [NSMutableArray arrayWithArray:self.results];
                        NSArray *tPtr2 = [ary.firstObject isKindOfClass:[ResponseData class]]?
                        ((ResponseData *)ary.firstObject).records:ary;
                        [t addObjectsFromArray:tPtr2];
                        self.results = [NSArray arrayWithArray:t];
                    }
                }
                else {
                    self.results = [NSArray arrayWithArray:ary];
                }
                //2015.08.27 Mike: Since the return is smaller than my request,
                //it is the end of data
                //for address api only as of now
                
                NSArray *dataArray;
                if ([ary.firstObject isKindOfClass:[ResponseData class]]) {
                    dataArray = ((ResponseData *)ary.firstObject).records;
                }
                else {
                    dataArray = ary;
                }
                if (dataArray.count < LOADINGBATCHSIZE) {
                    self.reachedTheEnd = YES;
                }
            }
            else {
                self.reachedTheEnd = YES;
                self.results = nil;
            }
            
            self.gettingMore = NO;
            self.updating = NO;
        }
    };
    //Get the default url
    NSString *urlString = self.myRepository.defaultListing;
    //Append any parameter if supplied
    if (para.length>0) {
        urlString = [urlString stringByAppendingString:para];
    }
    //Append the limit to the URL for testing
    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"&limit=%i", LOADINGBATCHSIZE]];
    //Append the offset to the URL
    urlString = [urlString stringByAppendingString:
                 [NSString stringWithFormat:@"&offset=%lu", (unsigned long)self.dataStorage.count]];
    //Using the helper to get object, and merge them
    [self privateGetDataByName:self.myRepository.ClassName usingURL:urlString completionBlock:update];
}

- (void)getDetailByID:(NSString *)ID {
    //Mark the flag
    self.updating = YES;
    //Block for getting the first batch of item
    void (^update)(NSArray *) = ^(NSArray *ary) {
        if (ary && ary.count > 0) {
            if ([ary.firstObject isKindOfClass:[ResponseData class]]) {
                self.results = [NSArray arrayWithArray:((ResponseData *)ary.firstObject).records];
            }
            else {
                self.results = [NSArray arrayWithArray:ary];
            }
        }
        else {
            self.results = nil;
        }
        self.updating = NO;
    };
    NSString *urlString = [NSString stringWithFormat:self.myRepository.detailsByID,ID];
    //Using the helper to get object, and merge them
    [self privateGetDataByName:self.myRepository.ClassName usingURL:urlString completionBlock:update];
}

- (NSArray *)dataStorage {
    NSArray *tPtr = [self.results.firstObject isKindOfClass:[ResponseData class]]?
    ((ResponseData *)self.results.firstObject).records:self.results;
    return tPtr;
}

@end
