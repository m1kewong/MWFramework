//
//  BaseModel.h
//
//  Created by Mike Wong on 8/6/13.
//  Copyright (c) 2013 Ricacorp. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MWRepository;

@interface MWBaseModel : NSObject

//Some basic flags
@property (nonatomic, getter = isUpdating) BOOL updating;
@property (nonatomic, getter = isGettingMore) BOOL gettingMore;
@property (nonatomic, getter = isReachedTheEnd) BOOL reachedTheEnd;

//The common data storage array
@property (nonatomic, getter=dataStorage) NSArray *dataStorage;

//Constructor
- (id)initWithRepository:(MWRepository *)repository
   withPackageIdentifier:(NSString *)identifier;

//Default provided methods
- (void)getDefaultListingWithExtraPara:(NSString *)para
                          fromBegining:(BOOL)isFromBegining;
- (void)getDetailByID:(NSString *)ID;

@end
