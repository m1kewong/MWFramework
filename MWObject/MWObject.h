//
//  rcObject.h
//  rcObject
//
//  Created by Mike Wong on 1/7/13.
//  Copyright (c) 2013 Ricacorp. All rights reserved.
//

#import <Foundation/Foundation.h>

//Abstract class to enforce all sub classes provide
//a function which will supply NSStrings name of the properties
//in order to rebuild the class from Json or XML
//The name of the property in the subclass must be the same as
//the name on server side, ie, the name must be the same as the key in json
@protocol MWAbstractObject

-(NSArray *)objectProperties;

//C
@optional
+(NSString *)postURLString;
//R
@optional
+(NSString *)listURLString;
@optional
+(NSString *)detailURLString;
//U
@optional
+(NSString *)updateURLString;
//D
@optional
+(NSString *)deleteURLString;

@end

//2015.03.27 Mike: This is a category to solve the error on 64 bit device,
//because setValue ends up calling longLongVaue defined in NSString
//but in the case of 64 bits it's calling unsignedLongLongValue which is undefined in NSString.
@interface NSString (unsignedlonglong)

- (unsigned long long)unsignedLongLongValue;

@end

@interface MWObject : NSObject <MWAbstractObject>
//A constructor that will rebuild the object from properties
//that are supplied by the abstract method objectProperties
//In practice, it can rebuild all non nested properties like string, float, integer
-(id)initWithAttributes:(NSDictionary *)attributes;

@end
