//
//  rcObject.m
//  rcObject
//
//  Created by Mike Wong on 1/7/13.
//  Copyright (c) 2013 Ricacorp. All rights reserved.
//

#import "MWObject.h"
#import <objc/runtime.h>

#define mustOverride() @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"%s must be overridden in a subclass/category", __PRETTY_FUNCTION__] userInfo:nil]
#define methodNotImplemented() mustOverride()

@implementation NSString (unsignedlonglong)

- (unsigned long long)unsignedLongLongValue {
    return self.longLongValue;
}

@end

@implementation MWObject

-(NSArray *)objectProperties {
    //By default, it print all the properties of that class
    //2015.12.16 Mike: v2 include super class as well
    Class clazz = [self class];
    unsigned int count;
    
    NSMutableArray *propertyArray = [[NSMutableArray alloc]init];
    BOOL stop = NO;
    while (!stop) {
        objc_objectptr_t *properties = (void *)class_copyPropertyList(clazz, &count);
        for (int i = 0; i < count; i++) {
            objc_objectptr_t property = properties[i];
            const char *propertyName = property_getName((objc_property_t)property);
            if (propertyName) {
                [propertyArray addObject:[NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
            }
        }
        free(properties);
        
        clazz = [clazz superclass];
        //Loop until I reach super class
        if (clazz == [NSObject class]) {
            stop = YES;
        }
    }
    
//    objc_objectptr_t *properties = (void *)class_copyPropertyList(clazz, &count);
//    NSMutableArray *propertyArray = [NSMutableArray arrayWithCapacity:count];
//    for (int i = 0; i < count; i++) {
//        objc_objectptr_t property = properties[i];
//        const char *propertyName = property_getName((objc_property_t)property);
//        if (propertyName) {
//            [propertyArray addObject:[NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
//        }
//    }
//    free(properties);
    return [NSArray arrayWithArray:propertyArray];
    
    //mustOverride(); // or methodNotImplemented(), same thing
}

-(id)initWithAttributes:(NSDictionary *)attributes {
    //mustOverride(); // or methodNotImplemented(), same thing
    self = [super init];
    if (self) {
        NSArray *t = [self objectProperties];
        for (NSString *key in t) {
            NSString *value = [attributes objectForKey:key];
            //Error checking
            if ((id)value == [NSNull null] || !value) {
                value = @"";
            }
            @try {
                [self setValue:value forKey:key];
            }
            @catch (NSException *exception) {
                NSLog(@"Fail to set a value for key %@", key);
            }
            @finally {
            }
        }
    }
    return self;
}

@end
