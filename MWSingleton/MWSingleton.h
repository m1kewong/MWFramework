//
//  rcSingleton.h
//  rcObject
//
//  Created by Mike Wong on 1/7/14.
//  Copyright (c) 2014 Ricacorp. All rights reserved.
//

//For subclass to create an instance
#define CREATE_INSTANCE                    \
+(id)sharedManager                         \
{                                          \
    static id theInstance = nil;           \
    if (theInstance == nil)                \
    {                                      \
        theInstance = [[self alloc] init]; \
        NSLog(@"Created %@", [self class]);\
    }                                      \
    return theInstance;                    \
}


#import <Foundation/Foundation.h>

@interface MWSingleton : NSObject

+ (id)sharedManager;

@end
