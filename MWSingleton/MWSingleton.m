//
//  rcSingleton.m
//  rcObject
//
//  Created by Mike Wong on 1/7/14.
//  Copyright (c) 2014 Ricacorp. All rights reserved.
//

#import "MWSingleton.h"

//static rcSingleton *sharedHelper = nil;

@implementation MWSingleton

#pragma mark Singleton Methods
+ (id)sharedManager {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass",
                                           NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

@end
