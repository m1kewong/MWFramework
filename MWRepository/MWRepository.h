//
//  MWRepository.h
//  MWFramework
//
//  Created by Mike Wong on 6/12/2016.
//  Copyright © 2016 Ricacorp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MWRepository : NSObject

//Name of the corresponding class, object in our convention,
//model in other documents, for reflection
@property (nonatomic, strong) NSString *ClassName;

//Data connection point, an API url

//GET
@property (nonatomic, strong) NSString *defaultListing;
@property (nonatomic, strong) NSString *detailsByID;

//POST
@property (nonatomic, strong) NSString *postByID;

//DELETE
@property (nonatomic, strong) NSString *deleteByID;

//UPDATE
@property (nonatomic, strong) NSString *updateByID;

- (id)initWithMWObjectClassName:(NSString *)name;

- (id)initWithName:(NSString *)clsname
          withList:(NSString *)list
        withDetail:(NSString *)detail
          withPost:(NSString *)post
        withDelete:(NSString *)delURL
        withUpdate:(NSString *)update;

@end
