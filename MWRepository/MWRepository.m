//
//  MWRepository.m
//  MWFramework
//
//  Created by Mike Wong on 6/12/2016.
//  Copyright © 2016 Ricacorp. All rights reserved.
//

#import "MWRepository.h"
#import "MWObject.h"

@implementation MWRepository

- (id)initWithMWObjectClassName:(NSString *)name {
    self = [super init];
    if (self) {
        Class model = NSClassFromString(name);
        self.ClassName = name;
        if ([model isSubclassOfClass:[MWObject class]]) {
            if ([model respondsToSelector:@selector(postURLString)] &&
                [model respondsToSelector:@selector(listURLString)] &&
                [model respondsToSelector:@selector(detailURLString)] &&
                [model respondsToSelector:@selector(updateURLString)] &&
                [model respondsToSelector:@selector(deleteURLString)]) {
                
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                self.postByID       = [model performSelector:@selector(postURLString)];
                self.defaultListing = [model performSelector:@selector(listURLString)];
                self.detailsByID    = [model performSelector:@selector(detailURLString)];
                self.updateByID     = [model performSelector:@selector(updateURLString)];
                self.deleteByID     = [model performSelector:@selector(deleteURLString)];
#pragma clang diagnostic pop
            }
        }
    }
    return self;
}

- (id)initWithName:(NSString *)clsname
          withList:(NSString *)list
        withDetail:(NSString *)detail
          withPost:(NSString *)post
        withDelete:(NSString *)delURL
        withUpdate:(NSString *)update {
    self = [super init];
    if (self) {
        self.ClassName      = clsname;
        self.defaultListing = list;
        self.detailsByID    = detail;
        self.postByID       = post;
        self.deleteByID     = delURL;
        self.updateByID     = update;
    }
    return self;
}

@end
